'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.addPatient = functions.https.onCall((data, context) => {
  const idPatient = data.idPatient || null;
  const idDoctor = data.idDoctor;
  const patientNom = data.patientNom;
  const patientPrenom = data.patientPrenom;
  const patientDateNaissance = data.patientDateNaissance;
  const patientGenre = data.patientGenre;
  let patientNumber = 0;
  if ( context.auth){
    return admin.database().ref('doctors/'+idDoctor+'/public/waiting').once('value', (snapshot) => {
      console.log(snapshot.val());
      patientNumber = +snapshot.val();
      patientNumber += 1;
    }).then(() => {
      console.log(patientNumber);
      if ( idPatient !== null){
        admin.database().ref('doctors/'+idDoctor+'/private/patients/today/en_attente/').push({
          patient:{
            patientNom, patientPrenom, patientDateNaissance, patientGenre, patientNumber,
            origin: 'app', status: '0'
          }
        }).then(() => {
          admin.database().ref('patients/'+idPatient).update({
            numero_list : patientNumber,
            rdv : idDoctor
          }).then(() => {
            admin.database().ref('doctors/'+idDoctor+'/public').update({
              waiting: patientNumber
            });
          });
        });
        
      } else{
        admin.database().ref('doctors/'+idDoctor+'/private/patients/today/en_attente/').push({
          patient:{
            patientNom, patientPrenom, patientDateNaissance, patientGenre, patientNumber,
            origin: 'web'
          }
        }).then(() => {
            admin.database().ref('doctors/'+idDoctor+'/public').update({
            waiting: patientNumber
          });
        });
      }
    });
    
  } 
  else{
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
      'while authenticated.');
  }
});

exports.deletePatient = functions.https.onCall((data, context) => {
  if ( context.auth ){
    const patientId = data.patientId;
    const patientPushId = data.patientPushId;
    const doctorId = data.doctorId;
    let userExist = false;
    console.log(data);
    let waiting = 0;
    return admin.database().ref('doctors/'+doctorId+'/public/waiting').once('value', (snapshot) => {
      waiting = +snapshot.val();
      waiting -= 1;
    }).then(() => {
      if ( patientId !== null ){
        admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId).once('value', (snapshot) => {
          userExist = snapshot.exists();
          console.log(userExist);
        }).then(() =>{
          if ( userExist ){
            admin.database().ref('patients/'+patientId).update({
              numero_list : '',
              rdv : '',
              canceled: 'true'
            }).then(() => {
              admin.database().ref('doctors/'+doctorId+'/public').update({
                waiting: waiting
              }).then(() => {
                admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId).remove();
              });
            });
          }
        });
      }
      else{
        admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId).once('value', (snapshot) => {
          userExist = snapshot.exists();
          if ( userExist ){
            admin.database().ref('doctors/'+doctorId+'/public').update({
              waiting: waiting
            }).then(() => {
              admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId).remove();
            });
          }
        });
      }
    })
  }
  else{
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
      'while authenticated.');
  }
});

exports.updateCurrentPatient = functions.https.onCall((data, context) => {
  const response = {
    patient_status : false,
    is_doctor: false
  };
  if ( context.auth ){
    const patientPushId = data.patientPushId;
    const doctorId = data.doctorId;
    let waiting = 0;
    let patientNumber = 0;
    let updated = false;
    if ( context.auth.uid === doctorId){
      response.is_doctor = true;
      admin.database().ref('doctors/'+doctorId+'/public').once('value', (snapshot) => {
        waiting = +snapshot.val().waiting;
        waiting -= 1;
      }).then(() => {
        admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId+'/patient').once('value',(snapshot) => {
          patientNumber = +snapshot.val().patientNumber;
          updated = snapshot.child('status').exists();
        }).then(() => {
          if ( updated ){
            return {success: false, info: 'user already updated'};
          } else{
            admin.database().ref('doctors/'+doctorId+'/public').update({
              current : patientNumber,
              waiting: waiting
            }).then(() => {
              admin.database().ref('doctors/'+doctorId+'/private/patients/today/en_attente/'+patientPushId+'/patient').update({
                status: 'en_cour'
              }).then(() => {
                response.patient_status = true;
              });
            });
            return {success: true, response};
          }
        });
        
      });
    }
    else {
      response.is_doctor = false;
    }
    return response;
  }
  else{
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
      'while authenticated.');
  }
});

exports.finalisePatient = functions.https.onCall((data, context) => {
  const patientNom = data.patientNom;
  const patientPrenom = data.patientPrenom;
  const patientGenre = data.genre;
  const patientDateNaissance = data.patientDateNaissance;
  const patientDiagnostique = data.diagnostique;
  const patientPrix = +data.prix;
  const patientDateRdv = data.dateRdv;
  const patientOrigin = data.origin;
  const patientPushId = data.pushId;
  const patientId = data.patientId || null;

  let stats = {
    origin: {
      web: 0,
      app: 0
    },
    genre: {
      homme: 0,
      femme: 0
    },
    totalPatients : 0,
    totalRevenues : 0
  };
  if ( context.auth ){
    admin.database().ref('doctors/'+context.auth.uid+'/private/stats').once('value',(snapshot) => {
      stats = snapshot.val();
    })
    .then(() => {
      stats.totalPatients += 1;
      stats.totalRevenues += patientPrix;    
      if ( patientGenre === 'homme' ){
        stats.genre.homme += 1;
      }
      else if ( patientGenre === 'femme' ){
        stats.genre.femme += 1;
      }
      if ( patientId !== null ){
        stats.origin.app += 1;
        admin.database().ref('doctors/'+context.auth.uid+'/private/patients/today/en_attente/'+patientPushId).once('value', (snapshot) =>{
          if ( snapshot.exists()){
            admin.database().ref('doctors/'+context.auth.uid+'/private/patients/today/en_attente/'+patientPushId).remove().then(() => {
              admin.database().ref('doctors/'+context.auth.uid+'/private/stats/').update({stats}).then(() => {
                admin.database().ref('patients/'+patientId).update({
                  rdv: '',
                  status: 'finished'
                }).then(() => {
                  admin.database().ref('doctors/'+context.auth.uid+'/private/archive').push({
                      patientNom, patientPrenom, patientGenre, patientDateNaissance, patientDiagnostique, patientPrix, patientDateRdv, patientOrigin
                  });
                })
              });
            });
            return {success: true, statistique : stats};
          }
          else{
            return {success: false, info: "user doesn't exist"};            
          }
        
      });
        
      }
      else{
        stats.origin.web += 1;
        admin.database().ref('doctors/'+context.auth.uid+'/private/patients/today/en_attente/'+patientPushId).once('value', (snapshot) =>{
          if ( snapshot.exists()){
            admin.database().ref('doctors/'+context.auth.uid+'/private/patients/today/en_attente/'+patientPushId).remove().then(() => {
              admin.database().ref('doctors/'+context.auth.uid+'/private/archive').push({
                patientNom, patientPrenom, patientGenre, patientDateNaissance, patientDiagnostique, patientPrix, patientDateRdv, patientOrigin
              }).then(() => {
                admin.database().ref('doctors/'+context.auth.uid+'/private/').update({stats}).then(() => {
                  return {success: true, statistique: stats};
                })
              });
            })
            return {success: true}
          } else{
            return {success: false, info: "user doesn't exist"};
          }
        })
        
    }
  });
    return {success: true};
  }
  else { 
    return {data: 'Error'};
  }
});